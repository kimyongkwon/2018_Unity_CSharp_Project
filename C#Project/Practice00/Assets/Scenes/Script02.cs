﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//------------
//  18.7.3
//  1.스크립트안에서 다른 오브젝트 찾기
//  2.property사용하기
//------------

public class TEST
{
    int a;

    // 원래 c++에서 Get,Set함수
    public void Set(int n)
    {
        a = n;
    }
    public int Get()
    {
        return a;
    }

    // Property기능
    public int A
    {
        get { return a; }
        set { a = value; }
    }
}

public class Script02 : MonoBehaviour {

    public GameObject FindObj = null;

    // #1-2
    // 찾는 방법2
    // 이렇게 선언해 놓고 엔진에서 Cylinder 오브젝트를 드래그로 끌어다 놔서 설정하기
    // 이 방법이 갖는 중요한 점은?
    // "엔진에서 적용한 것이 가장 최근에 적용된다"
    public GameObject FindObj2 = null;

    TEST t;
    
	// Use this for initialization
	void Start () {
        if (FindObj == null)
        {
            // #1-1
            // 찾는 방법1
            // Find함수와 오브젝트 이름을 사용해서 찾는다.
            // 하지만 많이 쓰는 방법은 아니다.
            FindObj = GameObject.Find("Cylinder");
            FindObj.transform.position = new Vector3(-1.0f, 2.0f, 3.0f);
        }

        // #2-1
        t = new TEST();
        // Property사용
        t.A = 100;
    }
	
	// Update is called once per frame
	void Update () {
    }
}
