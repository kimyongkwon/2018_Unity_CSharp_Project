﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//------------
//  18.7.3
//  스크립트안에서 또 다른 게임 객체 생성하기
//------------

public class Script00 : MonoBehaviour
{
    //public을 쓰면 외부에 노출된다.
    // #1 
    //객체를 가르킬? 변수를 public으로 선언           
    public GameObject obj = null;

    //스크립트안에서 다른 스크립트도 사용할 수 있다.
    public Script01 tmpscript = null;

    // Use this for initialization
    void Start()
    {
        if (obj == null)
        {
            // #2
            //obj객체가 null이면
            //GameObject를 하나 만들어 준다.
            //이렇게 만들어주면 디폴트로 transform정보만 가진 빈 게임객체(obj)가 생성된다.
            obj = new GameObject();

            // #3
            //obj객체의 transform컴포넌트를 가져와서(Get) position값 변경
            Transform GetTrans = obj.GetComponent<Transform>();
            GetTrans.transform.position = new Vector3(10f, 10f, 10f);

            // #4
            //obj에 Light컴포넌트를 추가
            //그리고 Light 컴포넌트를 가리키는 light변수 초기화
            Light light = obj.AddComponent<Light>();
            //이 light변수 type을 directional로 바꿔준다.
            if (light != null)
            {
                light.type = LightType.Directional;
            }

            // #5
            //obj에 Script01 스크립트를 컴포넌트로 추가시킨다.
            obj.AddComponent<Script01>();

            // #6
            //tmpscript가 null이면
            //obj가 가지고 있는 Script01 컴포넌트를 가져온다.
            if (tmpscript == null)
            {
                tmpscript = obj.GetComponent<Script01>();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        // #7
        //obj에 있는 tmpscript hp를 -1 한다.
        //그리고 만약 hp가 0이 된다면 
        //destroy한다.
        //tmpscript.hp -= 1;
        //if (tmpscript.hp == 0)
        //{
        //    GameObject.Destroy(tmpscript);
        //}

        // #8
        //또는 
        //오브젝트 자체를 삭제한다.
        tmpscript.hp -= 1;
        if (tmpscript.hp == 0)
        {
            //모든 스크립트는 자기를 가진 게임오브젝트의 레퍼런스를 가지고 있기 떄문에
            //스크립트를 통해 게임오브젝트를 삭제 할 수 있다.
            GameObject.Destroy(tmpscript.gameObject);
        }

        //Debug.Log("나는 Sciprt00");
    }
}
