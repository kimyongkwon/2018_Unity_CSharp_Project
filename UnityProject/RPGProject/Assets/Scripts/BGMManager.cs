﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BGMManager : MonoBehaviour {
    //BGM매니져는 게임에 하나만 존재해야한다.
    static public BGMManager instance;

    public AudioClip[] clips;
    private AudioSource source;

    private WaitForSeconds waitTime = new WaitForSeconds(0.01f);

    void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);         //"씬이 로드될때 해당객체를 지우지말아라"
            instance = this;
        }
    }

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
	}

    public void Play(int _playMusicTrack)
    {
        source.volume = 1f;
        source.clip = clips[_playMusicTrack];
        source.Play();
    }

    public void SetVolume(float _volume)
    {
        source.volume = _volume;
    }

    public void Pause()
    {
        source.Pause();
    }

    public void UnPause()
    {
        source.UnPause();
    }

    void Stop()
    {
        source.Stop();
    }

    IEnumerator FadeOutMusicCoroutine()
    {
        for (float i = 1.0f; i >= 0; i -= 0.01f)
        {
            source.volume = i;
            // new생성자가 for문안에 있으면 성능에 별로 좋지않다.
            // 그래서 위에서 미리 new생성자를 만들어준다.
            yield return waitTime;
        }
    }

    public void FadeOutMusic()
    {
        // 만약 Fadein과 Fadeout이 동시에 실행되면 꼬일수가 있다.
        // 그래서 지금 실행되고 있는 코루틴들을 전부 멈춘다.
        StopAllCoroutines();        
        StartCoroutine(FadeOutMusicCoroutine());
    }

    IEnumerator FadeInMusicCoroutine()
    {
        for (float i = 0.0f; i <= 1.0f; i += 0.01f)
        {
            source.volume = i;
            // new생성자가 for문안에 있으면 성능에 별로 좋지않다.
            // 그래서 위에서 미리 new생성자를 만들어준다.
            yield return waitTime;
        }
    }

    public void FadeInMusic()
    {
        // 만약 Fadein과 Fadeout이 동시에 실행되면 꼬일수가 있다.
        // 그래서 지금 실행되고 있는 코루틴들을 전부 멈춘다.
        StopAllCoroutines();
        StartCoroutine(FadeInMusicCoroutine());
    }

	// Update is called once per frame
	void Update () {
		
	}
}
