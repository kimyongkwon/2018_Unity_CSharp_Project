﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;              //SceneManager라는 함수를 쓰기위해 라이브러리를 불러옴


//----------------------------------------------------
//  
//  Name : TransferMap
//
//  Desc : Script for moving character to another map
//
//----------------------------------------------------

public class TransferMap : MonoBehaviour
{ 
    public GameObject StartPoint_InMap;
    private CameraManager theCamera;

    public BoxCollider2D targetBound;

    void Start()
    {
        theCamera = FindObjectOfType<CameraManager>();
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            theCamera.SetBound(targetBound);
            collision.gameObject.transform.position = new Vector3(StartPoint_InMap.gameObject.transform.position.x,
                StartPoint_InMap.gameObject.transform.position.y, collision.gameObject.transform.position.z);
            theCamera.transform.position = new Vector3(StartPoint_InMap.gameObject.transform.position.x,
                StartPoint_InMap.gameObject.transform.position.y, theCamera.transform.position.z);
        }
    }
}
