﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;              //SceneManager라는 함수를 쓰기위해 라이브러리를 불러옴


//----------------------------------------------------
//  
//  Name : TransferScene
//
//  Desc : 
//
//----------------------------------------------------

public class TransferScene : MonoBehaviour
{
    public string transferMapName;          //이동할 맵의 이름.
    private PlayerManager thePlayer;         //MovingObject의 CurrentMapName변수를 참조하기 위해서

    // Use this for initialization
    void Start()
    {
        thePlayer = FindObjectOfType<PlayerManager>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            thePlayer.currentMapName = transferMapName;

            //(6-1) 다른 씬으로 이동   
            SceneManager.LoadScene(transferMapName);
        }

    }
}
