﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TestMove
{
    public string name;
    public string direction;
}

public class Test : MonoBehaviour {

    [SerializeField]
    public TestMove[] move;                 
    //public string direction;
    private OrderManager theOrder;

	// Use this for initialization
	void Start () {
        theOrder = FindObjectOfType<OrderManager>();	
	}
	
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 충돌한 객체가 Player면 
        if (collision.gameObject.name == "Player")
        {
            // MovingObject인 객체를 모두 불러와서
            theOrder.PreLoadCharacter();
            //theOrder.Turn("npc1", direction);

            for (int i = 0; i < move.Length; i++)
            {
                // 해당 이름의 객체를 해당 방향으로 움직인다.
                theOrder.Move(move[i].name, move[i].direction);
            }
        }
    }

}
