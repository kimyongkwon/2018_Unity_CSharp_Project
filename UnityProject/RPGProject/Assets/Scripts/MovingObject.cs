﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------
//  
//  Name : MovingObject
//
//  Desc : Parent class that manages moving objects
//
//----------------------------------------------------

public class MovingObject : MonoBehaviour
{
    public string characterName;

    public float speed;
    public int walkCount;
    protected int currentWalkCount;

    private bool notCoroutine = false;          // 코루틴의 중복실행을 방지하기위해서
    // protected는 부모자식간에는 참조가 가능하지만 inspector창에서는 접근이 불가능하다.
    protected Vector3 vector;

    // part11 문제점1 : 캐릭터가 멈췄는데 계속해서 애니메이션이 실행된다.
    // 왜? : 캐릭터 Walking애니메이션이 끝나면 Walking애니메이션이 멈추고 Standing애니메이션이 실행되야하는데
    // 계속해서 Walking애니메이션이 실행된다.
    // 해결방법 : queue를 사용하였다. 캐릭터가 움직일 때마다 queue에 데이터를 인큐하고
    // 큐의 데이터를 하나씩 빼서 애니메이션(Walking) 및 움직임 실행 한다. 만약 큐의 데이터가 모두 디큐되었다면 애니메이션(Walking)을 종료한다.

    // part11 문제점2 : 만약 캐릭터가 위로 두번 이동한다면 평소보다 더 빠르게 위로 두칸 움직인다.
    // 왜? Move함수가 2번 연달아 호출되면 StartCoroutine이 2번보다 더 많이 실행되기때문에 캐릭터가 너무 빨리 움직인다. 
    // 해결방법 : notCoroutine이라는 변수를 사용해서 코루틴 안에서 queue가 다 비어질때까지 다른 코루틴은 실행되지 않게 한다.
    public Queue<string> queue;                 
    public BoxCollider2D boxCollider;
    public LayerMask layerMask;                 // 어떤레이어와 충돌했는지 판단하는 수
    public Animator animator;

    public void Move(string _dir, int _frequency = 5)
    {
        // Move함수가 실행될때마다 queue에는 _dir이 쌓이게 된다.
        queue.Enqueue(_dir);

        if (!notCoroutine)
        {
            notCoroutine = true;
            
            StartCoroutine(MoveCoroutine(_dir, _frequency));
        }
    }

    IEnumerator MoveCoroutine(string _dir, int _frequency)
    {
        // queue에 아무것도 없을떄까지 루프
        // 캐릭터가 멈췄는데 계속해서 애니메이션이 실행되는 문제점 때문에 queue를 사용하였다.
        while(queue.Count != 0)
        {
            switch (_frequency)
            {
                case 1:
                    yield return new WaitForSeconds(4f);
                    break;
                case 2:
                    yield return new WaitForSeconds(3f);
                    break;
                case 3:
                    yield return new WaitForSeconds(2f);
                    break;
                case 4:
                    yield return new WaitForSeconds(1f);
                    break;
                case 5:
                    break;
            }

            string direction = queue.Dequeue();
            vector.Set(0, 0, vector.z);

            switch (direction)
            {
                case "UP":
                    vector.y = 1f;
                    break;
                case "DOWN":
                    vector.y = -1f;
                    break;
                case "RIGHT":
                    vector.x = 1f;
                    break;
                case "LEFT":
                    vector.x = -1f;
                    break;
            }

            animator.SetFloat("DirX", vector.x);
            animator.SetFloat("DirY", vector.y);

            while (true)
            {
                bool checkCollisionFlag = CheckCollision();
                if (checkCollisionFlag)
                {
                    animator.SetBool("Walking", false);
                    yield return new WaitForSeconds(1f);
                }
                // 플레이어가 비킬 경우
                else
                {
                    break;
                }
            }
            
            animator.SetBool("Walking", true);

            // 움직이고자 하는 방향으로 boxcollider를 미리 움직인다.
            boxCollider.offset = new Vector2(vector.x * 0.7f * speed * walkCount, vector.y * 0.7f * speed * walkCount);

            while (currentWalkCount < walkCount)
            {
                transform.Translate(vector.x * speed, vector.y * speed, 0);
                currentWalkCount += 1;
                if (currentWalkCount == 12)
                    boxCollider.offset = Vector2.zero;
                yield return new WaitForSeconds(0.01f);       //0.01초동안 대기
            }

            currentWalkCount = 0;
            // 대기시간이 없을 때(_frequency == 5)는 계속해서 Walking 애니메이션이 실행되야한다.
            // 하지만 대기 시간(_frequency != 5)이 있을때는 Walking애니메이션이 멈춰야하기때문에 walking을 false한다.
            if (_frequency != 5)
                animator.SetBool("Walking", false);
        }
        // while문이 끝나면 애니메이션을 false시킨다(즉, queue가 비어있으면 애니메이션 중지)
        animator.SetBool("Walking", false);
        notCoroutine = false;
    }

    protected bool CheckCollision()
    {
        RaycastHit2D hit;
        // A지점, B지점
        // B지점을 향해 레이저를 쏴서 B지점에 도달한다면 hit = null; 
        // 만약 B지점까지 못가고 방해물에 부딪친다면 hit = 방해물

        Vector2 start = transform.position;                     // A지점, 캐릭터의 현재 위치 값
        Vector2 end = start + new Vector2(vector.x * speed * walkCount, vector.y * speed * walkCount); ;        //B지점, 캐릭터가 이동하고자 하는 위치 값

        boxCollider.enabled = false;                            // 레이저를 그냥 쏘게 되면 캐릭터 자신의 박스 콜라이더와 충돌하기때문에 레이저를 쏠때는 캐릭터의 박스 콜라이더를 잠깐 끈다.
        hit = Physics2D.Linecast(start, end, layerMask);        // 레이저를 쐇을때 start에서 end까지 도달하는지
        boxCollider.enabled = true;                             // 레이저를 쏘고 다시 킨다.

        if (hit.transform != null)      // A지점에서 B지점으로 레이저를 쐈을때 레이어마스크에 해당하는 벽이 있다면 이후의 코드는 실행하지 않겠다.
            return true;                // true가 반환된다는 것은 어떤 객체에 부딪친다는 의미다.
        return false;
    }
}
