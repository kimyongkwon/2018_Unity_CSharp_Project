﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------
//  
//  Name : PlayerManager
//
//  Desc : A class that manages player objects (inherits from MovingObject)
//
//----------------------------------------------------

// MovingObject를 상속받는다.
public class PlayerManager : MovingObject
{

    // 문제점 : school front를 갔다가 다시 start씬으로 오게되면 Player객체가 재생성된다.
    // 그러므로 재생성된 Player와 기존에 Player가 같이 존재되는 것이다. 
    // 해결방안 : 위 문제를 해결하기 위해 static를 정적변수를 사용한다. 
    // 이를 통해 해당 스크립트가 적용된 모든 객체(캐릭터)들은 static으로 선언된 변수의 값을 공유함.
    static public PlayerManager instance;

    public string currentMapName;               // transferMap 스크립트에 있는 transferMapName 변수의 값을 저장

    //(8)
    public string walkSound_1;
    public string walkSound_2;
    public string walkSound_3;
    public string walkSound_4;
    private AudioManager theAudio;

    public float runSpeed;
    private float applyRunSpeed;

    private bool applyRunFlag = false;          // 뛰는 속도를 컨트롤하기위한 변수

    private bool canMove = true;

    private void Awake()
    {
        // 처음 생성된 경우에만 instance의 값이 null이다.
        // 왜냐하면 생성된 이후에 this 값을 주었기 때문에
        // 그리고나서, 해당 스크립트가 적용된 객체가 또 생성될 경우
        // static으로 값을 공유한 instance의 값이 this이기 때문에 그 객체는 삭제됨.
        if (instance == null)
        {
            DontDestroyOnLoad(this.gameObject);         //"씬이 로드될때 해당객체를 지우지말아라"
            instance = this;
        }
        else
        {
            // DontDestroyOnLoad(this.gameObject)는 처음 생성된 Player객체, 
            // 다음 GameObject는 새로 생성되는 객체를 의미한다.
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        queue = new Queue<string>();
        boxCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        theAudio = FindObjectOfType<AudioManager>();
    }

    // 게임안에서는 캐릭터가 움직일때마다 한칸한칸씩 이동하게 만들어야 한다.(픽셀당 이동)
    // 만약 speed가 2.4, walkCount = 20 이라면
    // 2.4 * 20 = 48 , 즉 한번 방향키를 누를때마다 48픽셀씩 움직인다는 뜻이다.
    // 하지만 밑에와 같이 코드를 작성하면 너무 순식간에 48픽셀이 움직이기때문에 스무스하게 칸을 이동하지 않는다. 
    // 게다가 한번눌린 연산이 끝나기도 전에 다음키입력도 추가로 받아드리므로 여러칸을 이동하는 문제도 생긴다.
    // while(currentWalkCount<walkCount)
    // {
    //     2.4씩 움직이는 코드
    //     currentWalkCount += 1;
    // }
    // currentWalkCount = 0;
    // 이 상황을 해결하는 방법은 연산을 "대기"하는 것이다.
    // 이때 Coroutine을 사용한다(다른방법으로도 가능하긴하다)

    //코루틴
    IEnumerator MoveCoroutine()
    {
        while (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                applyRunSpeed = runSpeed;
                applyRunFlag = true;
            }
            else
            {
                applyRunSpeed = 0;
                applyRunFlag = false;
            }

            vector.Set(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), transform.position.z);

            //대각선의 키를 동시에 누르면 y값은 무시된다. 고로 이상하게 안움직임
            if (vector.x != 0)
                vector.y = 0;

            animator.SetFloat("DirX", vector.x);
            animator.SetFloat("DirY", vector.y);

            // CheckCollision함수의 반환값이 전달되고
            bool checkCollisionFlag = base.CheckCollision();
            // CheckCollision이 참이면 움직임을 멈춘다.
            if (checkCollisionFlag)
                break;

            animator.SetBool("Walking", true);

            int temp = Random.Range(1, 4);
            switch (temp)
            {
                case 1:
                    theAudio.Play(walkSound_1);
                    break;
                case 2:
                    theAudio.Play(walkSound_2);
                    break;
                case 3:
                    theAudio.Play(walkSound_3);
                    break;
                case 4:
                    theAudio.Play(walkSound_4);
                    break;
            }

            // 움직이고자 하는 방향으로 boxcollider를 미리 움직인다.
            boxCollider.offset = new Vector2(vector.x * 0.7f * speed * walkCount, vector.y * 0.7f * speed * walkCount);

            while (currentWalkCount < walkCount)
            {
                if (vector.x != 0)
                {
                    transform.Translate(vector.x * (speed + applyRunSpeed), 0, 0);
                }
                else if (vector.y != 0)
                {
                    transform.Translate(0, vector.y * (speed + applyRunSpeed), 0);
                }

                if (applyRunFlag)       //shift가 눌려있으면 currentWalkCount가 1 더 증가한다
                    currentWalkCount++;
                currentWalkCount++;
                if (currentWalkCount == 12)
                    boxCollider.offset = Vector2.zero;
                yield return new WaitForSeconds(0.01f);       //0.01초동안 대기


            }
            currentWalkCount = 0;
        }
        animator.SetBool("Walking", false);
        canMove = true;                 // 한번의 이동이 다 완료되면 canMove는 다시 true가 된다.
    }


    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            // Input.GetAxisRaw("Horizontal") => 우 방향키가 눌리면 1리턴, 좌 방향키가 눌리면 -1 리턴
            // Input.GetAxisRaw("Vertical") => 상 방향키가 눌리면 1리턴, 하 방향키가 눌리면 -1 리턴
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                canMove = false;        // 방향키가 한번 눌리면 canMove는 false가 된다. 
                                        // 이렇게한 이유는 방향키를 누르고 한번의 이동이 진행될때 코루틴들이 여러번 실행되지 않게 하기 위해서
                StartCoroutine(MoveCoroutine());
            }
        }
    }
}
