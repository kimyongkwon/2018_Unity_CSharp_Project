﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 커스텀클래스(NPCMove)가 inspector창에 뜨려면 다음과 같은 명령이 필요함
[System.Serializable]
public class NPCMove
{
    // 툴팁을 적어두면 inspector창에서 변수를 설명할 수 있다.
    [Tooltip("NPCMove를 체크하면 NPC가 움직임")]
    public bool NPCmove;

    public string[] direction;      // npc가 움직임 방향 설정

    // inspector창에 스크롤바를 하나 만들어준다.
    [Range(1,5)] [Tooltip("1 = 천천히, 2 = 조금 천천히, 3 = 보통, 4 = 빠르게, 5 = 연속적으로")]
    public int frequency;           // npc가 움직일 방향으로 얼마나 빠른 속도로 움직일 것인가.
}

//----------------------------------------------------
//  
//  Name : NPCManager
//
//  Desc : A class that manages NPC objects(inherits MovingObject)
//
//----------------------------------------------------

// MovingObject를 상속받는다.
public class NPCManager : MovingObject {
    
    // 커스텀클래스(NPCMove)가 inspector창에 뜨려면 다음과 같은 명령이 필요함
    [SerializeField]
    public NPCMove npc;

	// Use this for initialization
	void Start () {
        queue = new Queue<string>();
        StartCoroutine(MoveCoroutine());
    }

    public void SetMove()
    {
        
    }

    public void SetNotMove()
    {
        StopAllCoroutines();
    }

    IEnumerator MoveCoroutine()
    {
        if (npc.direction.Length != 0)
        {
            for (int i = 0; i < npc.direction.Length; i++)
            {
                
                yield return new WaitUntil(() => queue.Count < 2);

                // 위와 같은 문장이 없고, WaitForSeconds도 설정되어있지 않으면 대기없이 base.Move를 계속해서 실행시킨다(무한루프에 빠짐)
                // 실질적인 이동 구간
                base.Move(npc.direction[i], npc.frequency);

                // i가 Length길이만큼 되면 i를 -1로 초기화한다(NPC는 계속 움직인다)
                if ( i == npc.direction.Length - 1)
                    i = -1;
            }
        }
    }
}
