﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------
//  
//  Name : CameraManager
//
//  Desc : Character Info, Move
//
//----------------------------------------------------

public class CameraManager : MonoBehaviour {

    static public CameraManager instance;

    public GameObject target;               //카메라가 따라갈 대상
    public float moveSpeed;                 //카메라가 얼마나 빠른 속도로 따라갈것인지
    private Vector3 targetPosition;         //대상의 현재 위치 값

    public BoxCollider2D bound;

    // 박스 컬라이더 영역의 최소 최대 xyz값을 지님
    private Vector3 maxBound;
    private Vector3 minBound;

    // 카메라의 반너비, 반높이 값을 지닌 변수
    private float halfWidth;
    private float halfHeight;

    // 카메라의 반높이값을 구할 속성을 이용하기 위한 변수
    private Camera theCamera;

    // Start보다 먼저 실행되는 함수
    void Awake() {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);         //"씬이 로드될때 해당객체를 지우지말아라"
            instance = this;
        }
    }

    void Start() {
        theCamera = GetComponent<Camera>();
        minBound = bound.bounds.min;
        maxBound = bound.bounds.max;
        halfHeight = theCamera.orthographicSize;
        halfWidth = halfHeight * Screen.width / Screen.height;       
    }
  
	
	// Update is called once per frame
	void Update () {
        if (target.gameObject != null)
        {
            targetPosition.Set(target.transform.position.x, target.transform.position.y, this.transform.position.z);

            //lerp는 A값과 B값 사이의 선형보간으로 중간 값을 리턴한다. ex) (1, 10, 0.5f) = 5, (5, 10, 0.5f) = 7.5
            this.transform.position = Vector3.Lerp(this.transform.position, targetPosition, moveSpeed * Time.deltaTime);

            float clampedX = Mathf.Clamp(this.transform.position.x, minBound.x + halfWidth, maxBound.x - halfWidth);
            // ex) (10, 0, 100) 를 넣는다면 10이 리턴
            // ex) (-100, 0, 100) 를 넣는다면 0 리턴
            // ex) (1000, 0, 100) 을 넣는다면 100 리턴
            float clampedY = Mathf.Clamp(this.transform.position.y, minBound.y + halfHeight, maxBound.y - halfHeight);

            this.transform.position = new Vector3(clampedX, clampedY, this.transform.position.z);
        }
	}

    public void SetBound(BoxCollider2D newBound)
    {
        bound = newBound;
        minBound = bound.bounds.min;
        maxBound = bound.bounds.max; 
    }
}
