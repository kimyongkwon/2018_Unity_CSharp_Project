﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 커스텀 클래스는 public으로 선언해도 
// inspector창에 안뜬다.
// 그래서 다음과 같은 명령어를 써야한다.
[System.Serializable]

public class Sound
{
    public string name;             // 사운드의 이름
    public AudioClip clip;          // 사운드 파일
    private AudioSource source;     // 사운드 플레이어

    public float Volume;
    public bool loop;

    public void SetSource(AudioSource _source)
    {
        source = _source;
        source.clip = clip;
        source.loop = loop;
    }

    public void SetVolume()
    {
        source.volume = Volume;
    }

    public void Play()
    {
        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }

    public void SetLoop()
    {
        source.loop = true;
    }

    public void SetLoopCancel()
    {
        source.loop = false;
    }
}

public class AudioManager : MonoBehaviour {

    // 다음 명령어를 추가해야 inspector창에 커스텀클래스가 뜬다.
    [SerializeField]
    //사운드는 여러개이기 때문에 배열로 선언
    public Sound[] sounds;

    static public AudioManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);         //"씬이 로드될때 해당객체를 지우지말아라"
            instance = this;
        }
    }

    // Use this for initialization
    void Start () {
        for (int i = 0; i < sounds.Length; i++)
        {
            // 사운드게임객체를 만들고
            GameObject soundObject = new GameObject("사운드 파일 이름 : " + i + " = " + sounds[i].name);
            // 사운드게임객체에 AudioSource라는 컴포넌트를 추가한다. 그리고 그 오디오소스를 사운드배열에 설정한다.
            sounds[i].SetSource(soundObject.AddComponent<AudioSource>());

            // 만들어진 오디오게임오브젝트들이 AudioManager게임 객체의 자식이 되게 설정한다.
            // 이렇게 안하고 만약 사운드파일들이 많아지면 Hierarcky창이 더러워지니까..
            soundObject.transform.SetParent(this.transform);
        }
	}

    public void Play(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (_name == sounds[i].name)
            {
                sounds[i].Play();
            }
        }
    }

    public void Stop(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (_name == sounds[i].name)
            {
                sounds[i].Stop();
            }
        }
    }

    public void SetLoop(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (_name == sounds[i].name)
            {
                sounds[i].SetLoop();
            }
        }
    }

    public void SetLoopCancel(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (_name == sounds[i].name)
            {
                sounds[i].SetLoopCancel();
            }
        }
    }

    public void SetVolume(string _name, float _volume)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (_name == sounds[i].name)
            {
                sounds[i].Volume = _volume;
                sounds[i].SetVolume();
                return;
            }
        }
    }
}
