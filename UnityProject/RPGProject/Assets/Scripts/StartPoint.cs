﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------
//  
//  Name : StartPoint
//
//  Desc : Manage creation points when a character moves to the current map
//
//----------------------------------------------------

public class StartPoint : MonoBehaviour {

    public string startPoint;                           // 맵이 이동되면 플레이어가 시작될 위치
    private PlayerManager thePlayer;
    private CameraManager theCamera;

	void Start () {
        thePlayer = FindObjectOfType<PlayerManager>();
        theCamera = FindObjectOfType<CameraManager>();

		if (startPoint == thePlayer.currentMapName)
        {
            theCamera.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, theCamera.transform.position.z);
            thePlayer.transform.position = this.transform.position;
        }
	}

	void Update () {		
	}
}
