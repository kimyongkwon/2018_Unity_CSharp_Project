﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
    public float m_fPlayerInterval = 3.0f;
    public float m_fZPos = -10.0f;

    public GameObject PlayerObject = null;
	
	// Update is called once per frame
	void Update () {
        if(PlayerObject != null) { 
            transform.position = new Vector3(PlayerObject.transform.position.x, 
                0.0f, transform.position.z );
        }
    }
}
