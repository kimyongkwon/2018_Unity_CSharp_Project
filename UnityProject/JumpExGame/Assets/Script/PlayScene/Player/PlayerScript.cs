﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // 이동을 위해서는 스피드
    // 어떤 이동속도로 가겠느냐?
    public float m_fSpeed = 10.0f;
    public float m_fJumpPower = 15.0f;

    public int MaxJumpCount = 3;
    //현재 나의 점프가 몇번이나 남았나?
    public int CurJumpCount = 3;

    public Rigidbody m_Rigi = null;

    // 초기화 순서
    // 1. 리터럴 초기화가 가장먼저
    // MaxJumpCount = 3;
    // 2. 엔진초기화
    // MaxJumpCount = 5;
    // 3. 시점 함수 초기화
    // MaxJumpCount = 3;
    // 그럼 MaxJumpCount의 마지막 값은 3이된다.

    void JumpReset()
    {
        CurJumpCount = MaxJumpCount;
    }

    void Awake()
    {
        //위의 초기화 순서문제를 방지하기 위해 awake에서 CurJumpCount를 초기화한다.
        JumpReset();
        m_Rigi = GetComponent<Rigidbody>();         //gameObject.GetComponent
    }

    // Use this for initialization

    // Update is called once per frame
    void Update()
    {
        // 우리의 목표는 1초에 10으로 이동하고 싶다.
        // 하지만 다음과 같이 하면 눈깜짝할 사이에 물체가 이동한다
        //transform.position += new Vector3(m_fSpeed, 0.0f, 0.0f);

        // 왜?
        // 게임은 거의 대부분 운영체제의 한계 수준의 처리속력을 요구한다.
        // 그렇기때문에 1초에 수천 수만의 update를 실행할 것이다. 엄청 빠르다.
        // 게다가 만약 성능이 다른 컴퓨터가 두대 존재한다면
        // 어떤 컴퓨터는 1초에 수천번 update를 수행
        // 또 다른 컴퓨터는 1초에 수만번 update를 수행하기 떄문에 
        // 이말은 즉슨,
        // 1번 컴퓨터가 1초에 1번 update를 실행한다.
        // 2번 컴퓨터가 1초에 2번 update를 실행한다.
        // 그럼 어떻게 해야 함?
        // 답 : 델타 타임을 사용한다.
        // 한마디로 1초의 델타타임은 (1초 / 업데이트 실행횟수) 이다.

        // 1초에 1번 update를 실행하는 1번 컴퓨터는 델타타임을 1f 로 한다.
        // 1초에 2번 update를 실행하는 1번 컴퓨터는 델타타임을 0.5f 로 한다.
        // 그럼 1번 컴퓨터는 10 * 1f = 10 
        // 2번 컴퓨터는 10 * 0.5f = 5 이지만 update를 2번호출하는 컴퓨터이기때문에 5 * 2 = 10이 된다.    
        // transform.position += new Vector3(m_fSpeed * Time.deltaTime, 0.0f, 0.0f);

        // 키를 눌러 움직이겠다.
        //if (Input.GetKey(KeyCode.D))        //계속 눌리고 있을때
        //{
            transform.position += new Vector3(m_fSpeed * Time.deltaTime, 0.0f, 0.0f);
        //}
        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= new Vector3(m_fSpeed * Time.deltaTime, 0.0f, 0.0f);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += new Vector3(0.0f, m_fSpeed * Time.deltaTime, 0.0f);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= new Vector3(0.0f, m_fSpeed * Time.deltaTime, 0.0f);
        }

        //jump카운트가 남아있지 않으면 못뛴다.
        if (CurJumpCount != 0)
        {
            //n단 점프
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if ( m_Rigi != null)
                {
                    //addForce
                    //첫번쨰 의문 : 델타타임을 안곱해줄까?
                    //답 : 유니티의 물리는 모두 다 기본적으로 델타타임의 영향을 받기 때문에, 즉 벌써 다 곱해져있음.
                    //m_Rigi.AddForce(new Vector3(0.0f, m_fJumpPower, 0.0f));

                    //중력만 작용하는게 아니라, 공기저항력,마찰력,나의 질량 등의 영향을 받는다.
                    //add포스에는 이런 요소들을 무시하는 옵션이 존재한다.
                    //ForceMode.Impuse 기존 유니티의 여러가지 힘을 무시하고
                    //현재상태 그대로 쏘아 올린다.
                    //하지만 밑에 코드만 사용했을때 간과한것이 있다.
                    //바로 내가 한번 점프하고 중력으로인해 떨어질떄의 힘은 무시되지않는다는 것이다.
                    //점프하고 떨어질때의 힘이 작용한 상태에서 다시 점프를 하면 
                    //내가 의도했던 힘이 그대로 적용되지않는다.
                    //m_Rigi.AddForce(new Vector3(0.0f, m_fJumpPower, 0.0f),
                    //   ForceMode.Impulse);

                    //현재 가해지는 힘을 수정할 수 있다.
                    //아래의 코드는 점프하고 떨어질때의 힘이나 
                    //점프하는 동안 내게 가해지는 힘조차 모두 무시해버린다.
                    //그렇기때문에 점프키를 누를떄마다 균등하게 점프한다.
                    m_Rigi.velocity = Vector3.zero;     //m_Rigi.velocity = new Vector3(0.0f,0.0f,0.0f); 

                    m_Rigi.AddForce(new Vector3(0.0f, m_fJumpPower, 0.0f),
                       ForceMode.Impulse);
                }
                CurJumpCount -= 1;       
            }
        }
    }

    // 물리가 적용 안된 충돌
    void OnTriggerEnter(Collider _Col)
    {
        _Col.gameObject.SetActive(false);
    }

    // IsTrigger가 세팅되어있을떄와
    // 안되어 있을떄의 충돌 함수가 다르다.

    // OnTriggerEnter(처음 충돌했을떄)
    // OnTriggerStay(계속 충돌 중일떄)
    // OnTriggerExit(충돌에서 최초로 빠져나갔을때)

    // OnCollisionEnter
    // OnCollisionStay
    // OnCollisionExit
    // trigger와 Collision은 다르다.

    // 주의사항
    // 1. 이 함수는 리지드 바디를 가진 오브젝트의 스크립트에서만 실행된다.
    // 2. 콜라이드도 같이 가지고 있어야만 한다.
    // 3. collision을 인자값으로 받아야한다.
    // Collision _Col은 나와 충돌한 충돌체에서 세팅한 값이 들어오게 된다.

    // 물리가 적용된 충돌
    void OnCollisionEnter(Collision _Col)
    {
        // _Col을 구분할 필요가 없다. 무조건 판밖에 없으므로
        //나와 충돌한 게임오브젝트의 이름
        //Debug.Log(_Col.gameObject.name);
        //JumpReset();

        // 하지만 _Col을 구분할 필요가 있다면
        // 방법1. 만약 게임오브젝트 레이어를 설정했다면
        if (_Col.gameObject.layer == 10)
        {
            // 어떤 경우에만 리셋해줘야한다.
            // 내가 부딪친 곳이 바닥인지 아니면 윗면인지
            // 평면의 방정식(유니티에서 지원해줌)
            // 직선의 방정식

            // 평면을 구성하기 위해서 가장 기본적인것으로 2개의 벡터가 필요하다
            // 평면 위의 점과 
            Plane CheckPlane = new Plane(_Col.transform.up, _Col.transform.position);

            float Dis = CheckPlane.GetDistanceToPoint(transform.position);

            if (Dis > 0f)
            { 
                Debug.Log("윗면");
                JumpReset();
            }
            else
            {
                Debug.Log("아랫면");
            }
        }
    }
}
