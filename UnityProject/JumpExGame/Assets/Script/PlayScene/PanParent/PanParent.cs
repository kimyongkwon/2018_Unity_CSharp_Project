﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanParent : MonoBehaviour {

    //판이 몇개가 생겨야 할까?
    public int PanCount = 30;
    public GameObject PanPrefab = null;
    // c#에서 배열이란 무조건 동적배열이다. 
    // 그 자체로 이미 클래스다.
    public GameObject[] ArrPan = null;
    public Vector3 LastPanPos = Vector3.zero;

    public int CointCount = 0;
    public GameObject CoinPrefab = null;
    public GameObject[] ArrCoin = null;

    public GameObject Player = null; 

    void Awake()
    {
        // 리소스라는 클래스는 이미 알고 있다.
        // 어차피 할 수 있는게 리소스 폴더 아래있는 녀석들만 로드할 수 있다.
        PanPrefab = Resources.Load<GameObject>("Prefab/ActivePan");
        CoinPrefab = Resources.Load<GameObject>("Prefab/Coin");

        if (PanPrefab == null)
            return;
        if (CoinPrefab == null)
            return;
        if (Player == null)
            return;

        LastPanPos = Player.transform.position;

        // GameObject를 30개 만들었다는 것이 아니다.
        // GameObject를 담을 수 있는 30개의 칸을 만들어놨다.
        ArrPan = new GameObject[PanCount];
        for ( int i = 0; i < ArrPan.Length; i++)
        {
            ArrPan[i] = GameObject.Instantiate<GameObject>(PanPrefab);
            // 부모자식관계는 오브젝트간의 관계라기보다는
            // 트랜스폼의 관계에 더 가깝다.
            // 자기의 부모가 될 트랜스 폼을 넣어달라
            ArrPan[i].transform.SetParent(gameObject.transform);

            // 끄고 켜는 기능, false는 끄는 기능
            // 현재 아무것도 안하는 객체들은 켜질필요가 없다.
            ArrPan[i].SetActive(false);
        }

        CointCount = PanCount * 9;
        ArrCoin = new GameObject[CointCount];
        for (int i = 0; i < ArrCoin.Length; i++)
        {
            ArrCoin[i] = GameObject.Instantiate<GameObject>(CoinPrefab);
            ArrCoin[i].transform.SetParent(transform);
            ArrCoin[i].SetActive(false);
        }
    }

    int m_CurActivePan = 0;
    int m_CurActiveCoin = 0;

    // 플레이어의 위치를 계속 파악하면서 판을 앞에 하나씩 놔줘야 한다.
    // 마찬가지로 그때마다 뒤에있는 판을 하나씩 앞으로 옮겨야한다.
    // 어떤 기준에 의해서 할것인가
    void Update()
    {
        if ( LastPanPos.x <= Player.transform.position.x + 20.0f)
        {
            ArrPan[m_CurActivePan].SetActive(true);

            Vector3 RandomPos;
            RandomPos.x = LastPanPos.x + Random.Range(5f, 15f);
            RandomPos.y = Random.Range(-5f, 5f);
            RandomPos.z = 0f;

            Vector3 RandomRot;
            RandomRot.x = 0f;
            RandomRot.y = 0f;
            RandomRot.z = Random.Range(-40f, 40f);

            ArrPan[m_CurActivePan].transform.position = RandomPos;
            // 유니티의 회전값은 기본적으로 사원수인 쿼터니언을 사용한다.
            ArrPan[m_CurActivePan].transform.eulerAngles = RandomRot;

            LastPanPos = RandomPos;

            // 판 위에 Coin을 놔주기 위해서는 바로 y값을 더해주면 안돼고(판은 회전하기때문에)
            // 판의 up vector를 더해줘야한다.
            // 또한 up vector는 물체가 원점에 있을 떄의 up벡터이다.
            // 그러므로 판 위치에 up벡터를 더해줘야한다.
            Vector3 StartPanCoinPos = LastPanPos;
            StartPanCoinPos += ArrPan[m_CurActivePan].transform.up;
            StartPanCoinPos -= ArrPan[m_CurActivePan].transform.right * 4;
           
            for (int i = 0; i < 9; i++)
            {
                ArrCoin[m_CurActiveCoin].SetActive(true);
                ArrCoin[m_CurActiveCoin].transform.position = StartPanCoinPos;
                StartPanCoinPos += ArrPan[m_CurActivePan].transform.right;
                m_CurActiveCoin += 1;
            }

            m_CurActivePan += 1;

            if (m_CurActivePan >= PanCount)
                m_CurActivePan = 0;

            if (m_CurActiveCoin >= CointCount)
                m_CurActiveCoin = 0;
        }
    }
}
