﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//접근제한 지정자
//기본적으로 public은 공개한다는 뜻
//MonoBahaviour 클래스를 상속했음
//이때 c#은 상속할떄 무조건 public으로만 상속된다.(private, protected는 없음)

//sealed 키워드는 상속할수 없다는 뜻

//게임에서는 기본적으로 루프를 사용한다

//이때 유니티는 이녀석의 메모리를 만들때 클래스의 이름과 파일이름을 매칭시켜서 만든다.
//클래스명과 이 클래스를 담고 있는 파일명(스크립트명)이 다르다면
//실행이 안되거나 오작동 할 수도 있고
//기본적으로 오브젝트에 스크립트를 넣어줄수 없다.
public class TestScript : /*(public)*/MonoBehaviour
{
    public TestScript1 m_newTestScript1 = null;
    public Transform GetTrans = null;

    //오브젝트에 접근하는 방법2
    //전역에서 Findobj를 null로 초기화해주고
    //엔진에서 Capsule 대입
    public GameObject Findobj = null;

    private int m_TestSCNumber = 10;
    public int TestSCNumber
    {
        get
        {
            return m_TestSCNumber;
        }
        set
        {
            if ( 0 > value )
            {
                return;
            }

            m_TestSCNumber = value;         //여기서 value는 TestSCNumber의 자료형과 같다.
        }
    }

    //메모리에 올라왔을때의 한번 실행하는 코드
    //유니티의 시점함수라고 한다.
    //특정 시점에서 특정 이름의 함수를 
    //유니티 엔진이 자동으로 실행해준다.
    //일반적으로 이런 시점함수는 오브젝트안에 넣어줘야만 동작을 한다.
    void Awake()
    {
        // Debug.Log("정말 한번만 메모리에 올라왔을때 글자");

        //c#에서는 다음이 가능하다, int도 클래스이기 때문에.
        //int number = 10;
        //number.ToString();

        //아무것도 없는 빈 오브젝트 만드는 법
        //먼저 오브젝트 만드는 법
        //유니티는 씬파일을 통해 씬의 마지막 구성요소를 저장하고 있다.
        //new GameObject();               //아무것도 없는 빈 오브젝트(만약 이게 Update함수에 있다면?) 

        //실행되었을떄 메모리에 올라온 씬과
        //파일로서의 씬은 다른 씬이다.
        //c#의 new라는 것은
        //레퍼런스가 생긴다.
        //c++에서는 포인터가 생긴다고 new 포인터가 기본적인 개념이고,
        //힙에 올라간 메모리를 직접 안지워줘도 된다.
        //왜? 카비지 컬렉터라는 것이 레퍼런스 카운팅방식으로 알아서 지워준다.

        GameObject NewObject = new GameObject();
        //NewObject.ToString();
        //NewObject->ToString();        //c#은 -> 접근이 안됌 c#은 .밖에 없다.

        //게임오브객체에 Light컴포넌트를 추가한다.
        //c++에서는 템플릿 함수
        //c#에서는 제네릭이라고 부른다.
        Light TempLight = NewObject.AddComponent<Light>();
        if (null != TempLight)
        {
            TempLight.type = LightType.Directional;
        }

        //내가 만든 클래스도 언제든지 넣어줄수 있다.
        m_newTestScript1 = NewObject.AddComponent<TestScript1>();

        //static 함수
        //GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);

        //컴포넌트에 접근하는 법
        //안에 이미 들어와 있는 녀석들 중에서 Transform이라는 
        //녀석이 존재한다면 Transform을 리턴해달라
        GetTrans = obj.GetComponent<Transform>();
        if ( GetTrans != null )
        {
            GetTrans.position = new Vector3(2, 2, 2);
        }

        //트랜스폼은 어떤 오브젝트에 들어가고
        //항상 정말 많이 사용하는 컴포넌트이다.
        //누군가는 이렇게 물어볼수 있다. 
        //데이터를 외부에 공개한것이 아니냐? 하지만 c#에서는 프로퍼티라는 문법이 존재한다.
        //멤버변수로 받지 않아도 될 정도면 그냥 사용하고
        obj.transform.position = new Vector3(-2, -2, -2);

        //오브젝트에 접근하는 밥법1
        //GameObject Findobj = GameObject.Find("Capsule");        //잘 안쓰임. 왜? 모든 오브젝트를 다 찾기때문에

        if (null != Findobj)
        {
            Findobj.transform.position = new Vector3(-1.0f, 5.0f, 3.0f);
        }
    }

    // Use this for initialization
    void Start()
    {
        //여러분이 눈에 보이는 컴포넌트
        //이름을 쳐보면 그녀석은 모두 클래스일 확률이 굉장히 높다

        //스타트는 한번만 된다.직
        //이 오브젝트가 만들어지고, 게임에서 메모리상에 올라가게 되면
        //조건에 따라서 한번만 실행되게 된다.
        //일반적으로 한번만 실행된다.
        //Debug.Log("글자");
    }

    // Update is called once per frame
    void Update()
    {
        GetTrans.position += new Vector3(1, 0, 0);
        
        if (null != m_newTestScript1)
        {
            Debug.Log(m_newTestScript1.Hp);
            m_newTestScript1.Hp = m_newTestScript1.Hp - 1;

            if ( 0 == m_newTestScript1.Hp )
            {
                //모든 스크립트들은 자신의 내부에
                //자기를 가진 게임오브젝트의 레퍼런스를 가지고 있다.

                //GameObject에서 기본적으로 지원해주는 파괴함수
                //게임오브젝트를 넣으면 게임오브젝트가 삭제된다.
                //GameObject.Destroy(m_newTestScript1.gameObject);
                //컴포넌트를 넣는다면 컴포넌트가 삭제되고
                GameObject.Destroy(m_newTestScript1);

                m_newTestScript1 = null;
            }
        }

        //Debug는 느리다. 그렇기떄문에 if문 안에서 쓰는게 성능에 좋을 수 있다.
        // Debug.Log("계속나오는 글자");
    }
}
