﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript1 : MonoBehaviour {

    //접근제한 지정자를 다 붙여줘야한다.
    //public은 외부에 다 공개
    public int Hp = 500;

    //private은 공개하지않음
    //private int PrivateInt = 999;

    //어트리뷰트라는 문법이 존재한다.
    //private이면서도 엔진 공개가 가능해진다.
    [SerializeField]
    private int PrivateInt = 999;

    void Awake()
    {
        TestScript Number = null;
        if(null != Number)
        {
            Number.TestSCNumber = 100;
        }
    }
}
